import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-component',
  templateUrl: './form-component.component.html',
  styleUrls: ['./form-component.component.css']
})
export class FormComponentComponent implements OnInit {
  httpOptions = {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'POST',
      'Access-Control-Allow-Headers': 'Content-Type, Authorization',
    }
  };
  shipmentForm: FormGroup;
  btnAdd = false;
  baseUrl = 'http://localhost:8081';
  shipmentRequest: any;
  submitted = false;
  loading = false;
  loadingAdd = false;
  addSuccess = false;
  message = '';
  loadingForm = false;
  returnUrl = '';
  shipmentInfo: any;

  constructor(private router: Router, private http: HttpClient, private formBuilder: FormBuilder) {
  }

  get f() {
    return this.shipmentForm.controls;
  }

  ngOnInit() {
    this.shipmentForm = this.formBuilder.group({
      sender_name: ['La Redoute', [Validators.required]],
      sender_email: ['laredoute@example.com', [Validators.required, Validators.email]],
      sender_phone: ['07 12 34 56 78', [Validators.required]],
      sender_address: ['Boulevard de Lafayette', [Validators.required]],
      sender_locality: ['Paris', [Validators.required]],
      sender_postal_code: ['75001', [Validators.required]],
      sender_country_code: ['FR', [Validators.required]],
      receiver_name: ['Marquise de Pompadou', [Validators.required]],
      receiver_email: ['marquise-de-pompadou@example.com', [Validators.required]],
      receiver_phone: ['07 98 76 54 32', [Validators.required]],
      receiver_address: ['175 Rue de Rome', [Validators.required]],
      receiver_locality: ['Marseille', [Validators.required]],
      receiver_postal_code: ['13006', [Validators.required]],
      receiver_country_code: ['FR', [Validators.required]],
      height: [100, [Validators.required]],
      width: [50, [Validators.required]],
      length: [20, [Validators.required]],
      dimension_unit: ['cm', [Validators.required]],
      weight_amount: [2500, [Validators.required]],
      weight_unit: ['g', [Validators.required]]
    });
  }

  getQuote() {
    this.submitted = true;
    this.loading = true;
    if (this.shipmentForm.invalid) {
      this.loading = false;
    }
    const shipmentInput = this.shipmentForm.value;
    const body = this.getBody(shipmentInput);
    this.shipmentInfo = body.data;
    console.log(this.shipmentInfo);
    const url = this.baseUrl + '/client/getquote';
    this.http.post(url, body/* , this.httpOptions */).subscribe(
      (res: any) => {
        let data: any;
        data = res.data[0];
        if (data.amount > 0) {
          this.shipmentInfo.cost = data.amount;
          console.log(this.shipmentInfo.cost);
          this.shipmentInfo.quote.id = data.id;
          this.btnAdd = true;
          this.loading = false;
          this.message = `Your shipment cost is ${res.data[0].amount} EUR.
          Please click the button below to create your shipment order.`;
        } else {
          this.btnAdd = false;
          this.message = `Cannot get the shipping cost. Please verify your information.`;
        }
      }
    );
  }

  createShipment() {
    this.submitted = true;
    this.loadingAdd = true;
    if (this.shipmentForm.invalid) {
      this.loadingAdd = false;
      return;
    }
    const shipmentInput = this.shipmentForm.value;
    const body = this.getBody(shipmentInput);
    this.shipmentInfo = body.data;
    const url = this.baseUrl + '/client/createshipment';
    this.http.post(url, body, this.httpOptions)
      .subscribe((res: any) => {
        let data: any;
        data = res.data;
        if (data.cost > 0) {
          this.message = 'Register shipment success';
        } else {
          this.message = 'Register shipment Error';
        }
        setTimeout(() => {
          this.loadingAdd = false;
          this.router.navigate([this.returnUrl]);
        }, 2000);
      });
  }

  backGetQuote() {
    this.btnAdd = false;
  }

  getBody(requestBody) {
    return {
      data: {
        quote: {
          id: '',
        },
        origin: {
          contact: {
            name: requestBody.sender_name,
            email: requestBody.sender_email,
            phone: requestBody.sender_phone
          },
          address: {
            country_code: requestBody.sender_country_code,
            locality: requestBody.sender_locality,
            postal_code: requestBody.sender_postal_code,
            address_line1: requestBody.sender_address
          }
        },
        destination: {
          contact: {
            name: requestBody.receiver_name,
            email: requestBody.receiver_email,
            phone: requestBody.receiver_phone
          },
          address: {
            country_code: requestBody.receiver_country_code,
            locality: requestBody.receiver_locality,
            postal_code: requestBody.postal_code,
            address_line1: requestBody.receiver_address
          }
        },
        package: {
          dimensions: {
            height: requestBody.height,
            width: requestBody.width,
            length: requestBody.length,
            unit: requestBody.dimension_unit
          },
          grossWeight: {
            amount: requestBody.weight_amount,
            unit: requestBody.weight_unit,
          }
        }
      }
    };
  }

}
